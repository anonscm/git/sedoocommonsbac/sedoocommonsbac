package fr.sedoo.commons.ioc;

public interface BeanFactory 
{	
	Object getBeanByName(String beanName);
}
