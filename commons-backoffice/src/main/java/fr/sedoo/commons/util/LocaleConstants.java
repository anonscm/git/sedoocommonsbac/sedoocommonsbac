package fr.sedoo.commons.util;

public interface LocaleConstants {
	
	public static final String FRENCH = "FR";
	public static final String ENGLISH = "EN";
	public static final String SPANISH = "ES";
	public static final String PORTUGUESE = "PT";
}
