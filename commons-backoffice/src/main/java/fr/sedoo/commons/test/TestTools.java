package fr.sedoo.commons.test;

import java.io.InputStream;

import org.apache.commons.io.IOUtils;

public class TestTools {

	public static String getResourceFileContent(Class clazz, String fileName) throws Exception
	{
		InputStream resourceAsStream = clazz.getResourceAsStream(fileName);
		String content = IOUtils.toString(resourceAsStream, "UTF-8");
		return content;
	}
	
}
