package fr.sedoo.commons.spring;

import fr.sedoo.commons.ioc.BeanFactory;

public class SpringBeanFactory implements BeanFactory {

	@Override
	public Object getBeanByName(String beanName) 
	{
		return ApplicationContextProvider.getContext().getBean(beanName);
	}

}
